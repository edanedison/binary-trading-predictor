const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BlockSchema = new Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    min: {
        type: Number
    },
    max: {
        type: Number
    },
    range: {
        type: Number
    },
    mean: {
        type: Number
    },
    median: {
        type: Number
    },
    mode: {
        type: Number
    },
    standardDev: {
        type: Number
    },
});

module.exports = mongoose.model('Block', BlockSchema);