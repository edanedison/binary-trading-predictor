const numbers = require('numbers');
const colors = require('colors');
const Block = require('../models/block');

let ticks = [],
    tickCount = 0,
    tickInterval = 10,
    direction,
    higher,
    lower,
    eventChain = 0,
    betLow = false,
    betHigh = false,
    block = [],
    prevBlock = [],
    bets = 0,
    wins = 0,
    movementForBet = 1,
    showResult = false;

let self = module.exports = {

    events: function(liveApi, track) {

        console.log(('Tracking: ' + track + ' in blocks of ' + tickInterval + ' ticks\n').yellow);

        liveApi.events.on('tick', function(response) {

            ticks.push(parseFloat(response.tick.quote).toFixed(3));

            let current = ticks[ticks.length - 1]
            let previous = ticks[ticks.length - 2]

            switch (true) {
                case current > previous:
                    direction = ' ↑ '.green
                    break;
                case current < previous:
                    direction = ' ↓ '.red
                    break;
                default:
                    direction = ' ~ '.blue
            }

            console.log(current + direction)

            tickCount++

            if (tickCount >= tickInterval) {

                block = ticks.slice(-tickInterval, -1);
                block.push(ticks[ticks.length - 1]);

                switch (true) {
                    case block.slice(-1)[0] > block[0]:
                        direction = 'Block finished on a HIGHER value than it started'.green
                        higher = true;
                        if (lower === true) {
                            eventChain = 0;
                            lower = false;
                        }
                        eventChain++;
                        break;
                    case block.slice(-1)[0] < block[0]:
                        direction = 'Block finished on a LOWER value than it started'.red
                        lower = true;
                        if (higher === true) {
                            eventChain = 0;
                            higher = false;
                        }
                        eventChain++;

                        break;
                    default:
                        direction = 'Block finished on the SAME value as it started'.blue
                        eventChain = 0;
                        lower = false;
                        higher = false;
                }

                if (eventChain < 0) {
                    eventChain = 0;
                }

                console.log('');
                console.log('Range: ' + parseFloat(numbers.basic.max(block.map(Number)) - numbers.basic.min(block.map(Number))).toFixed(3));
                console.log('Mean: ' + numbers.statistic.mean(block.map(Number)).toFixed(3));
                console.log('Median: ' + numbers.statistic.median(block.map(Number)).toFixed(3));
                console.log('Mode: ' + numbers.statistic.mode(block.map(Number)).toFixed(3));
                console.log('Std Dev: ' + numbers.statistic.standardDev(block.map(Number)).toFixed(3));

                console.log('');
                console.log(direction);
                // console.log('Momentum: ' + eventChain);
                console.log('-----------------------------------');

                var blockDb = new Block({
                    min: numbers.basic.min(block.map(Number)).toFixed(3),
                    max: numbers.basic.max(block.map(Number)).toFixed(3),
                    range: parseFloat(numbers.basic.max(block.map(Number)) - numbers.basic.min(block.map(Number))).toFixed(3),
                    mean: numbers.statistic.mean(block.map(Number)).toFixed(3),
                    median: numbers.statistic.median(block.map(Number)).toFixed(3),
                    mode: numbers.statistic.mode(block.map(Number)).toFixed(3),
                    standardDev: numbers.statistic.standardDev(block.map(Number)).toFixed(3),
                });

                blockDb.save(function(err) {
                    if (err) throw err;
                });

                // console.log(prevBlock.slice(-1)[0] + " to " + block.slice(-1)[0])

                if (betHigh && block.slice(-1)[0] > prevBlock.slice(-1)[0]) {
                    console.log('Won'.green);
                    wins++
                    eventChain = 0;
                    showResult = true;
                } else if (betLow && block.slice(-1)[0] < prevBlock.slice(-1)[0]) {
                    console.log('Won'.green);
                    wins++
                    eventChain = 0;
                    showResult = true;
                } else if (betHigh && block.slice(-1)[0] < prevBlock.slice(-1)[0]) {
                    console.log('Lost because end of current block was LOWER than end of last block'.red);
                    eventChain = 0;
                    showResult = true;
                } else if (betLow && block.slice(-1)[0] > prevBlock.slice(-1)[0]) {
                    console.log('Lost because end of current block was HIGHER than end of last block'.red);
                    eventChain = 0;
                    showResult = true;
                } else if ((betLow && block.slice(-1)[0] === prevBlock.slice(-1)[0]) || (betLow && block.slice(-1)[0] === prevBlock.slice(-1)[0])) {
                    console.log('Lost because end of current block was SAME as end of last block'.red);
                    eventChain = 0;
                    showResult = true;
                }

                if (wins < 0) {
                    wins = 0;
                }

                if (eventChain > movementForBet && lower === true) {
                    console.log("This block should finish HIGHER than " + block.slice(-1)[0] + "\n".blue)
                    betHigh = true;
                    betLow = false;
                    bets++
                } else if (eventChain > movementForBet && higher === true) {
                    console.log("This block should finish LOWER than " + block.slice(-1)[0] + "\n".blue)
                    betHigh = false;
                    betLow = true;
                    bets++
                } else {
                    betLow = false;
                    betHigh = false;
                    //  console.log("Hold your horses...".red)
                }

                if (showResult) {
                    console.log(bets + " bet(s) / " + wins + " win(s)")
                    console.log((parseInt((wins / bets) * 100) || 0) + '% win rate');
                    console.log('\n');
                    showResult = false;
                }

                tickCount = 0;

                prevBlock = block;

            }

        });


    }

}