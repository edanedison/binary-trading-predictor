# Binary Stock Trading Predictor

Uses the binary.com API to track indices and then make predictions on the data. Currently predictions use a basic formula, but I plan to implement a neural network to look for more sophisticated patterns.  

## Requirements

- node / npm / mongo

## Installation

1. npm install
2. node server.js

## Dependencies

1. Node > 7.0 (Async support)
2. Mongo