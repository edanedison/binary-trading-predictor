const express = require('express');
const path = require('path');
const Block = require('../models/block');

const api = express.Router();

// GET method to return all items...

api.get('/blocks/', (req, res, next) => {
    Block.find(req.query).lean().exec((err, documents) => {
        if (err) return next(err);
        res.json(documents);
    });
});

// GET method to return blocks for today

api.get('/today/', (req, res, next) => {

    let start = new Date();
    start.setSeconds(0);
    start.setHours(0);
    start.setMinutes(0);

    let end = new Date(start);
    end.setHours(23);
    end.setMinutes(59);
    end.setSeconds(59);

    req.query["createdAt"] = { $gt: start, $lt: end };

    Block.find(req.query).lean().exec((err, documents) => {
        if (err) return next(err);
        res.json(documents);
    });
});


module.exports = api;