// Require our dependencies
const path = require('path')
const express = require("express");
const http = require('http');
const app = express();
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const bodyParser = require('body-parser');
const cors = require('cors');
const socketio = require('socket.io');
const server = http.Server(app);
const io = socketio(server);
const ws = require('ws');
const LiveApi = require('binary-live-api').LiveApi;

const api = require('./api');
const utils = require('./utils');
const port = process.env.PORT || 8080;

// Mongoose setup

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/binary-wizard', {
    useMongoClient: true,
    promiseLibrary: require('bluebird')
});

const liveApi = new LiveApi({
    websocket: ws,
    appId: 4863
});

const token = 'qNeg3oQzZe9OBg4';
const track = 'frxAUDJPY';

const init = async() => {

    await liveApi.authorize(token);
    console.log('Authenticated....\n')
    
    liveApi.events.on('transaction', function(response) {
        console.log(response);
    });

    liveApi.getPortfolio(true);
    liveApi.subscribeToTick(track);    
            
    utils.events(liveApi, track);

};

// App setup

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.use('/api', api);

app.set('json spaces', 2); // Pretty printing

server.listen(port, () => {
    console.log('Listening on port: ' + port);
    init();
});